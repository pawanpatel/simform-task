import { success } from "../../helpers/responseTemplates"
import db from '../../config/mongoConfig'

export default (request, response) => {
    db.forms.find().sort({ "createdOn": -1 }, (err, result) => {
        if (err)
            internalServerError(response, err)
        else
            success(response, result)
    })
}