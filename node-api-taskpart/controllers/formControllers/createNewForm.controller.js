import db from "../../config/mongoConfig"
import { badRequest, internalServerError, success } from "../../helpers/responseTemplates"
import { v4 } from 'uuid'

export default (request, response) => {
    const { data } = request.body
    if (data) {
        db.forms.insert({ ...data, key: v4(), createdOn: new Date().getTime() }, (err, doc) => {
            if (err)
                internalServerError(response, err)
            else
                success(response, doc)
        })
    }
    else {
        badRequest(response, { message: "Bad request" })
    }
}