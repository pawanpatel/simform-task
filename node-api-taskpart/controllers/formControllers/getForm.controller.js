import { badRequest, internalServerError, success } from "../../helpers/responseTemplates"
import db from '../../config/mongoConfig'
export default (request, response) => {
    const { formId } = request.params
    if (formId) {
        db.forms.findOne({ key: formId }, (err, result) => {
            if (err)
                internalServerError(response, err)
            else if (result?.name)
                success(response, result)
            else
                internalServerError(response, { message: "not found" })
        })
    }
    else {
        badRequest(response, { message: "Bad Request" })
    }
}